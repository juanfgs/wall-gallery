#!/usr/bin/env python3

import sys
import gi
import os
gi.require_version("Gtk", "3.0")
from gi.repository import GLib,Gio,Gtk

class Application(Gtk.Application):
    window = None
    sources = []
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="com.juanfgs.wall_gallery",
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            **kwargs
        )
        base_path = os.path.abspath(os.path.dirname(__file__))
        resource_path = os.path.join(base_path, 'resources/ui.gresource')
        self.resource = Gio.Resource.load(resource_path)
        self.resource._register()
        self.add_main_option(
            "test",
            ord("t"),
            GLib.OptionFlags.NONE,
            GLib.OptionArg.NONE,
            "Command line test",
            None,
        )

    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        #       builder = Gtk.Builder.new_from_string(MENU_XML, -1)
#        self.set_app_menu(builder.get_object("app-menu"))

    def do_activate(self):
        # We only allow a single window and raise any existing ones
        #
        if not self.window:
            from ui.app_window import AppWindow
            self.window = AppWindow(application=self)
        self.window.present()

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()
        # convert GVariantDict -> GVariant -> dict
        options = options.end().unpack()

        if "test" in options:
            # This is printed on the main instance
            print("Test argument recieved: %s" % options["test"])

        self.activate()
        return 0

    def on_about(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self.window, modal=True)
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()

if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)
