#!/usr/bin/env python3
from abc import ABC,abstractmethod,abstractproperty
import requests

class Source(ABC):
    _cursor = 1
    _auth = {}
    _url = ""
    def reset(self):
        self.cursor = 1

    @property
    def url(self):
        return self._url
    @property
    def auth(self):
        return self._auth
    @auth.setter
    def auth(self,value):
        if not value:
            raise ValueError("Authentication data should not be empty")
        self._auth = value

    @abstractmethod
    def get_images(self,options):
        pass
    @abstractmethod
    def get_image(self,image_id):
        pass
    @abstractmethod
    def search(self,term):
        pass
    @abstractmethod
    def next(self,page):
        pass
    @abstractmethod
    def prev(self,page):
        pass
    def _request(self,verb,parameters = {}):
        response = requests.get("/".join( ( self.url, verb ) ) , parameters)
        if response.ok:
            return response.json()
        else:
            raise SourceException("Api returned error")

class SourceException(Exception):
    pass
