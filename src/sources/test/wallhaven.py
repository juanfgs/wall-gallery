#!/usr/bin/env python3

import unittest
import json
from pprint import pprint
from nose.tools import assert_is_not_none
from unittest.mock import Mock,patch
from sources.wallhaven import Wallhaven
from sources.source import SourceException

class TestWallhaven(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.wallhaven = Wallhaven()

    def test_url(self):
        self.assertEqual("https://wallhaven.cc/api/v1", self.wallhaven.url)
    @patch('sources.source.requests.get')
    def test_get_images_should_return_list(self,mock_get):
        response = open("./src/sources/test/mocks/search.json", "r")
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = json.load(response)
        results = self.wallhaven.get_images()
        self.assertIsInstance(results["data"], list)

    @patch('sources.source.requests.get')
    def test_get_images_should_raise_exception(self,mock_get):
        mock_get.return_value.ok = False
        self.assertRaises(SourceException, self.wallhaven.get_images)

    def test_sets_auth_data(self):
        auth_data = {
            'apikey': 'dummykey',
        }
        self.wallhaven.auth = auth_data
        self.assertIn(auth_data['apikey'],self.wallhaven.auth.values())

    @patch('sources.source.requests.get')
    def test_get_image_should_return_valid_image(self,mock_get):
        response = open("./src/sources/test/mocks/image.json", "r")
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = json.load(response)
        results = self.wallhaven.get_image("z8mq8y")
        self.assertIn("path",results["data"], "Image Path not found")
        self.assertEqual("z8mq8y",results["data"]["id"], "Id mismatch")

    @patch('sources.source.requests.get')
    def test_next_should_fetch_more_records(self,mock_get):
        first_response = open("./src/sources/test/mocks/search.json", "r")
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = json.load(first_response)
        results1 = self.wallhaven.get_images()
        second_response = open("./src/sources/test/mocks/page2.json", "r")
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = json.load(second_response)
        results2 = self.wallhaven.next()
        self.assertTrue(results1["meta"]["current_page"] < results2["meta"]["current_page"])
