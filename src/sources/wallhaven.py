#!/usr/bin/env python3
from sources.source import Source,SourceException
from pprint import pprint

class Wallhaven(Source):
    _url = 'https://wallhaven.cc/api/v1'

    def search(self):
        pass
    def get_image(self,id):
        return self._request("w/" + id)
    def get_images(self):
        return self._request("search",{})
    def next(self):
        self._cursor += 1
        return self._request("search", { "page" : self._cursor })
    def prev(self):
        self._cursor -= 1
        return self._request("search", { "page" : self._cursor })
