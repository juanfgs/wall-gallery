#!/usr/bin/env python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository  import Gtk
from sources.wallhaven import Wallhaven
from pprint import pprint

@Gtk.Template.from_resource("/ui/mainwindow.ui")
class AppWindow(Gtk.ApplicationWindow):
    __gtype_name__ = "MainWindow"
    images = []
    sources = []
    WallpaperGrid : Gtk.Box = Gtk.Template.Child()
    def __init__(self, application):
        super().__init__(application=application)
        self.sources.append(Wallhaven())
        for source in self.sources:
            self.images.append(source.get_images()['data'])

        for image in self.images:
            self.WallpaperGrid.add( Gtk.Image.new_from_file(image['path']))
