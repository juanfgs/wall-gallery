#!/usr/bin/env python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository  import Gtk

@Gtk.template.from_resource("resources/ui/mainwindow.ui")
class AppWindow(Gtk.ApplicationWindow):
    def __init__(self, application):
        super().__init__(application)
        pass
